import React from "react"
import ContentLoader from "react-content-loader"

const Skeleton = (props) => (
  <ContentLoader 
    speed={2}
    width={410}
    height={500}
    viewBox="0 0 410 500"
    backgroundColor="#FFF3DC"
    foregroundColor="#FED59C"
    {...props}
  >
    <rect x="10" y="10" rx="5" ry="5" width="200" height="40" /> 
    <rect x="215" y="10" rx="5" ry="5" width="200" height="40" /> 
    <rect x="10" y="60" rx="5" ry="5" width="200" height="40" /> 
    <rect x="215" y="60" rx="5" ry="5" width="200" height="40" /> 
    <rect x="11" y="110" rx="5" ry="5" width="200" height="40" /> 
    <rect x="215" y="110" rx="5" ry="5" width="200" height="40" /> 
    <rect x="11" y="157" rx="5" ry="5" width="200" height="40" /> 
    <rect x="216" y="158" rx="5" ry="5" width="200" height="40" /> 
    <rect x="11" y="202" rx="5" ry="5" width="200" height="40" /> 
    <rect x="216" y="202" rx="5" ry="5" width="200" height="40" /> 
    <rect x="11" y="252" rx="5" ry="5" width="200" height="40" /> 
    <rect x="216" y="252" rx="5" ry="5" width="200" height="40" /> 
    <rect x="12" y="302" rx="5" ry="5" width="200" height="40" /> 
    <rect x="216" y="302" rx="5" ry="5" width="200" height="40" /> 
    <rect x="12" y="349" rx="5" ry="5" width="200" height="40" /> 
    <rect x="217" y="350" rx="5" ry="5" width="200" height="40" /> 
    <rect x="113" y="402" rx="5" ry="5" width="200" height="40" />
  </ContentLoader>
)
const MyLoader = (props) => (
    <ContentLoader 
      speed={2}
      width={410}
      height={500}
      viewBox="0 0 410 500"
      backgroundColor="#FFF3DC"
      foregroundColor="#FED59C"
      {...props}
    >
      <rect x="113" y="402" rx="5" ry="5" width="200" height="40" /> 
      <rect x="5" y="10" rx="0" ry="0" width="400" height="380" />
    </ContentLoader>
  )
  
  export default MyLoader
//export default Skeleton;