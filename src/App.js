import React, { useEffect, useRef, useState } from 'react';
import axios from 'axios';
import MyLoader from './components/skeleton';
import { Document, Page, pdfjs } from 'react-pdf';
//import pdf from './file.pdf';
import Annotation from './components/annotation';
//import Footer from './components/footer';
//import './App.css';
pdfjs.GlobalWorkerOptions.workerSrc = `//cdnjs.cloudflare.com/ajax/libs/pdf.js/${pdfjs.version}/pdf.worker.js`;

const serverAddress = 'https://docxtemplate.onrender.com/';
function App() {
  const [selectedFile, setSelectedFile] = useState(null);
  const [responseData, setResponseData] = useState(null);
  const [isLoading, setIsLoading] = useState(false);
  const [isDownloading, setIsDownloading] = useState(false);
  const [dataLength, setDataLength] = useState(0);
  const [error, setError] = useState(null);

  const inputRefs = useRef({});

  useEffect(() => {
    if (selectedFile) handleSubmit();
  }, [selectedFile]);

  useEffect(() => {
    if (responseData)
      setDataLength(Object.keys(responseData).length)
  }, [responseData]);

  const handleFileSelect = async (event) => {
    const file = event.target.files[0];
    setSelectedFile(file);
  };
  const handlePatch = async () => {
    const data = {};
    setIsDownloading(true);
    Object.keys(inputRefs.current).forEach(key => {
      data[key] = inputRefs.current[key].value;
    });
    const jsonData = JSON.stringify(data);
    const formData = new FormData();
    formData.append('file', selectedFile);
    formData.append('patches', JSON.stringify(data));

    try {
      const response = await axios.post(`${serverAddress}patch`, formData, {
        responseType: 'blob',
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      });
      const blob = new Blob([response.data], { type: 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' });

      // Создаем ссылку для скачивания
      const downloadUrl = window.URL.createObjectURL(blob);
      const link = document.createElement('a');
      link.href = downloadUrl;
      link.setAttribute('download', 'file.docx'); // Название файла
      document.body.appendChild(link);
      link.click();

      // Удаляем ссылку после скачивания
      link.parentNode.removeChild(link);
    } catch (error) {
      //console.log('Ошибка:', error);
    } finally {
      setIsDownloading(false); // Stop loading irrespective of result
    }
  };
  const handleSubmit = async () => {
    if (selectedFile) {
      setIsLoading(true);
      setError(null);
      const formData = new FormData();
      formData.append('file', selectedFile);

      try {
        const response = await axios.post(`${serverAddress}upload`, formData, {
          headers: {
            'Content-Type': 'multipart/form-data'
          }
        });
        setResponseData(response.data);
      } catch (error) {
        //console.log('Ошибка:', error);
        setError('Произошла ошибка при обработке файла. Скорее всего ваш файл .docx только снаружи, а на самом деле древний, никому не нужный и всеми забытый .doc. Можете упаковать его в zip и попробовать снова, должно сработать');
        if (error.response) {
          // Дополнительные действия в случае ответа сервера с ошибкой
          //console.log('Данные ошибки:', error.response.data);
         // console.log('Статус ошибки:', error.response.status);
        }
      } finally {
        setIsLoading(false); // Stop loading irrespective of result
      }
    }
  };

  return (
    <div className='flex-col flex min-h-screen h-screen bg-gradient-to-r from-light-sand to-grad-finish'>
      <div className="flex flex-1 flex-row">
        <div className='overflow-auto flex-2 m-5 min-w-[420px] max-w-[420px] w-[420px]'>
          <input
            id="fileInput"
            type="file"
            accept=".docx"
            onChange={handleFileSelect}
            className='border border-blue-900 p-3 mt-10 ml-10block w-full text-xl text-blue-950
          file:mr-4 file:py-2 file:px-4
          file:rounded-full file:border-0
          file:text-sm file:font-semibold
          file:bg-blue-950 file:text-white
          hover:file:bg-blue-800'
          />


          {isLoading ? (
            <div className="loader"><MyLoader /></div> // Render loading indicator
          ) : (

            <div className='flex flex-col items-center  w-full text-center'>
              {error && <div className='text-red-500 text-xl'>{error}</div>}
              {responseData && (

                dataLength > 0 ? (// Проверяем наличие и длину объекта responseData
                  <>
                    <table className='mt-10'>
                      <tbody>
                        {Object.entries(responseData).map(([key, value], index) => (
                          <tr key={index} className='border'>
                            <td className='px-4 py-2'>
                              <label>{value}:</label>
                            </td>
                            <td className='px-4 py-2'>
                              <input ref={el => inputRefs.current[value] = el} type="text" defaultValue='' />
                            </td>
                          </tr>
                        ))}
                      </tbody>
                    </table>
                    <button onClick={handlePatch} disabled={isDownloading} className='text-white bg-blue-950 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium text-3xl rounded-lg 
                                              px-5 py-2.5 mr-2 mt-5 focus:outline-none disabled:bg-gray-800'>{!isDownloading ? 'Заменить' : 'Скачивается'}</button>
                  </>
                ) : (
                  <div className='text-blue-950 text-xl'>В загруженом вами файле нет разметки шаблона, попробуйте что-нибудь еще</div>
                )
                // <div className='text-blue-950 text-xl'>В загруженом вами файле нет разметки шаблона, попробуйте что-нибудь еще</div> // Отображаем этот div, если в responseData нет элементов
              )}
            </div>
          )}
        </div>
        <div className='border flex-1 text-center text-white text-2xl items-center align-middle justify-center flex'>

          <Annotation />

        </div>
      </div>
      {/* <Footer /> */}
    </div>
  );
}

export default App;