module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    extend: {
      colors: {
        'sand': '#FED59C', // Replace with the exact sand color you want
        'dark-blue': '#0B2632', 
        'light-sand': '#FFF3DC',
        'light-blue': '#7A979D',// Replace with the exact dark blue you want 
        'grad-start': '#C2D5E8',
        'grad-finish': '#EDEFF6',
      },
    },
  },
  plugins: [],
}
